﻿using BaseProject.Infrastructure.Enums;

namespace BaseProject.Services.Dto.v1.UserDto
{
	public class InsertUserRequestDto
	{
		public string Name { get; set; }

		public Sex Sex { get; set; }

		public string PhoneNumber { get; set; }

		public string Email { get; set; }

		public string Username { get; set; }

		public string Password { get; set; }
	}
}
