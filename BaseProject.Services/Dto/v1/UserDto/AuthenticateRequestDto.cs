﻿namespace BaseProject.Services.Dto.v1.UserDto
{
    public class AuthenticateRequestDto
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
