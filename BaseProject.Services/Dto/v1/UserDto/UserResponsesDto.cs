﻿using System;
using BaseProject.Infrastructure.Enums;

namespace BaseProject.Services.Dto.v1.UserDto
{
    public class UserResponsesDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public Sex Sex { get; set; }

        public string Role { get; set; }
    }
}
