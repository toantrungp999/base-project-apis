﻿using BaseProject.Services.Dto;
using FluentValidation;

namespace BaseProject.Services.Validators
{
	public class PaginationRequestDtoValidator : AbstractValidator<PaginationRequestDto>
	{
		public PaginationRequestDtoValidator()
		{
			RuleFor(x => x.PageIndex).GreaterThanOrEqualTo(0);
			RuleFor(x => x.PageSize).GreaterThan(0);
		}
	}
}
