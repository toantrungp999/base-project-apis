﻿using BaseProject.Services.Dto.v1.UserDto;
using FluentValidation;

namespace BaseProject.Services.Validators.v1.User
{
    public class UserInsertDtoValidator : AbstractValidator<InsertUserRequestDto>
    {
        public UserInsertDtoValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty();

            RuleFor(x => x.Email).MinimumLength(6).MaximumLength(255).EmailAddress().NotNull().NotEmpty();

            RuleFor(x => x.PhoneNumber).MinimumLength(6).MaximumLength(12).NotNull().NotEmpty();

            RuleFor(x => x.Username).MinimumLength(6).MaximumLength(255).NotNull().NotEmpty();

            RuleFor(x => x.Password).MinimumLength(6).MaximumLength(255).NotNull().NotEmpty();
        }
    }
}
