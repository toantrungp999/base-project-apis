﻿using BaseProject.Services.Dto.v1.UserDto;
using FluentValidation;

namespace BaseProject.Services.Validators.v1.User
{
    public class AuthenticateDtoValidator : AbstractValidator<AuthenticateRequestDto>
    {
        public AuthenticateDtoValidator()
        {
            RuleFor(x => x.Username).MinimumLength(6).MaximumLength(255).NotNull().NotEmpty();

            RuleFor(x => x.Password).MinimumLength(6).MaximumLength(255).NotNull().NotEmpty();
        }
    }
}
