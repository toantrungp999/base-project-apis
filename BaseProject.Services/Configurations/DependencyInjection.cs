﻿using Microsoft.Extensions.DependencyInjection;
using NetCore.AutoRegisterDi;

namespace BaseProject.Services.Configurations
{
	public static class DependencyInjection
	{
		/// <summary>
		/// Configure the dependency injection for Services.
		/// </summary>
		public static IServiceCollection ConfigureServiceDi(this IServiceCollection services)
		{
			services.RegisterAssemblyPublicNonGenericClasses()
				.Where(c => c.Name.EndsWith("Service"))
				.AsPublicImplementedInterfaces();

			return services;
		}
	}
}
