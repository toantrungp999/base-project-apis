﻿using System.Reflection;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace BaseProject.Services.Configurations
{
	/// <summary>
	/// Configure the fluent validation.
	/// </summary>
	public static class FluentValidation
	{
		public static void ConfigureFluentValidation(this IServiceCollection services)
		{
			services.AddFluentValidation(cfg => cfg.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));
		}
	}
}
