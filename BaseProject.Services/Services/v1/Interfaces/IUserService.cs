﻿using System;
using BaseProject.Services.Dto;
using BaseProject.Services.Dto.v1.UserDto;

namespace BaseProject.Services.Services.v1.Interfaces
{
	public interface IUserService
	{
		UserResponsesDto GetUserById(Guid userId);

		PaginationDto<UserResponsesDto> GetUsers(PaginationRequestDto paginationRequest);

		AuthenticateResponsesDto Authenticate(AuthenticateRequestDto authenticateRequest);

		Guid InsertUser(InsertUserRequestDto request);
	}
}
