﻿using AutoMapper;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Constants;
using BaseProject.Infrastructure.Exceptions;
using BaseProject.Infrastructure.Persistence.Repositories;
using BaseProject.Infrastructure.Persistence.UnitOfWork.Interfaces;
using BaseProject.Infrastructure.Utilities;
using BaseProject.Services.Dto.v1.UserDto;
using BaseProject.Services.Services.v1.Interfaces;
using NetCore.AutoRegisterDi;
using System;
using System.Linq;
using BaseProject.Infrastructure.Extensions;
using BaseProject.Services.Dto;
using BaseProject.Services.Extensions;
using System.Security.Claims;
using BaseProject.Infrastructure.Configurations;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace BaseProject.Services.Services.v1
{
    [RegisterAsScoped]
    public class UserService : IUserService
    {
        private readonly IUnitOfWorkProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly IRepository<User> _useRepository;
        private readonly AppSettings _appSettings;

        public UserService(IUnitOfWorkProvider uowProvider,
            IMapper mapper,
            IRepository<User> useRepository,
            IOptions<AppSettings> appSettings)
        {
            _uowProvider = uowProvider;
            _mapper = mapper;
            _useRepository = useRepository;
            _appSettings = appSettings.Value;
        }

        public UserResponsesDto GetUserById(Guid userId)
        {
            using IUnitOfWorkScope scope = _uowProvider.Provide();

            var user = _useRepository.Get(userId);

            Guard.ThrowIfNull<NotFoundException>(user, string.Format(ExceptionConstant.NotFound, nameof(User)));

            return _mapper.Map<UserResponsesDto>(user);
        }

        public PaginationDto<UserResponsesDto> GetUsers(PaginationRequestDto paginationRequest)
        {
            using IUnitOfWorkScope scope = _uowProvider.Provide();

            var users = _useRepository.GetAll(true)
                .Paginate<User, UserResponsesDto>(_mapper, paginationRequest.PageIndex, paginationRequest.PageSize);

            return users;
        }

        public AuthenticateResponsesDto Authenticate(AuthenticateRequestDto authenticateRequest)
        {
            using IUnitOfWorkScope scope = _uowProvider.Provide();

            var user = _useRepository.GetAll(true).FirstOrDefault(
                           x => (x.UserName.ContainKeyWordInvariant(authenticateRequest.Username) || x.Email.ContainKeyWordInvariant(authenticateRequest.Username))
                            && x.Password == authenticateRequest.Password.ConvertToMD5());

            Guard.ThrowIfNull<NotFoundException>(user, ExceptionConstant.LoginFail);

            var claims = new List<Claim>();

            claims.AddRange(new[]
            {
                  new Claim(ClaimTypes.Name, user.Id.ToString()),
                  new Claim(ClaimTypes.Role, user.Role)
            });

            var jwtToken = JwtTokenHelper.GenerateToken(_appSettings.Secret, claims);

            AuthenticateResponsesDto result = _mapper.Map<AuthenticateResponsesDto>(user);

            result.Token = jwtToken;

            return result;
        }

        public Guid InsertUser(InsertUserRequestDto request)
        {
            using IUnitOfWorkScope scope = _uowProvider.Provide();

            var userByUserName = _useRepository.GetAll(true).FirstOrDefault(
                            x => x.UserName.ContainKeyWordInvariant(request.Username));

            Guard.ThrowByCondition<BusinessLogicException>(userByUserName != null, ExceptionConstant.UserNameExist);

            var userByEmail = _useRepository.GetAll(true).FirstOrDefault(
                           x => x.Email.ContainKeyWordInvariant(request.Email));

            Guard.ThrowByCondition<BusinessLogicException>(userByEmail != null, ExceptionConstant.EmailExist);

            var userByPhoneNumber = _useRepository.GetAll(true).FirstOrDefault(
                           x => x.PhoneNumber.ContainKeyWordInvariant(request.PhoneNumber));

            Guard.ThrowByCondition<BusinessLogicException>(userByPhoneNumber != null, ExceptionConstant.PhoneNumberExist);

            var entity = new User
            {
                Name = request.Name,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                Sex = request.Sex,
                UserName = request.Username,
                Password = request.Password.ConvertToMD5()
            };

            _useRepository.Add(entity);

            scope.Complete();

            return entity.Id;
        }
    }
}
