﻿using AutoMapper;
using BaseProject.Domain.Entities;
using BaseProject.Services.Dto.v1.UserDto;

namespace BaseProject.Services.AutoMapper.v1
{
	public class MappingProfile	: Profile
	{
		public MappingProfile()
		{
			MapUser();
		}

		private void MapUser()
		{
			CreateMap<User, UserResponsesDto>();
			CreateMap<User, AuthenticateResponsesDto>();
		}
	}
}
