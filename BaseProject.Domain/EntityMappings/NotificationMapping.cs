﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class NotificationMapping : EntityTypeConfigurationDependency<Notification>
    {
        private readonly AppSettings _appSettings;

        public NotificationMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Notifications), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
