﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class FanpageMapping : EntityTypeConfigurationDependency<Fanpage>
    {
        private readonly AppSettings _appSettings;

        public FanpageMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Fanpage> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Fanpages), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
