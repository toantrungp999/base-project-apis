﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class GroupMapping : EntityTypeConfigurationDependency<Group>
    {
        private readonly AppSettings _appSettings;

        public GroupMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Groups), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
