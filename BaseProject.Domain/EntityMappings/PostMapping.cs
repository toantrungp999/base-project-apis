﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class PostMapping : EntityTypeConfigurationDependency<Post>
    {
        private readonly AppSettings _appSettings;

        public PostMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Posts), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
