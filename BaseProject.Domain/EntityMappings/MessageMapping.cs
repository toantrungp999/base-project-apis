﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class MessageMapping : EntityTypeConfigurationDependency<Message>
    {
        private readonly AppSettings _appSettings;

        public MessageMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Messages), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
