﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class AttachmentMapping : EntityTypeConfigurationDependency<Attachment>
    {
        private readonly AppSettings _appSettings;

        public AttachmentMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Attachment> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Attachments), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
