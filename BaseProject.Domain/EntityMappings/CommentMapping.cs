﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class CommentMapping : EntityTypeConfigurationDependency<Comment>
    {
        private readonly AppSettings _appSettings;

        public CommentMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Comments), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
