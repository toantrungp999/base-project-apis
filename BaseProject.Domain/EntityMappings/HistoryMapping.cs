﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class HistoryMapping : EntityTypeConfigurationDependency<History>
    {
        private readonly AppSettings _appSettings;

        public HistoryMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<History> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Historys), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
