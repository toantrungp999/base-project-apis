﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class EntityMapping : EntityTypeConfigurationDependency<Entity>
    {
        private readonly AppSettings _appSettings;

        public EntityMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Entity> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Entitys), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
