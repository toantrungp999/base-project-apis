﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class UserMapping : EntityTypeConfigurationDependency<User>
    {
        private readonly AppSettings _appSettings;

        public UserMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Users), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
