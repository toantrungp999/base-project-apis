﻿using BaseProject.Domain.Context;
using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.EntityMappings
{
    public class MessengerMapping : EntityTypeConfigurationDependency<Messenger>
    {
        private readonly AppSettings _appSettings;

        public MessengerMapping(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public override void Configure(EntityTypeBuilder<Messenger> builder)
        {
            builder.ToTable(nameof(BaseProjectContext.Messengers), _appSettings.SchemaSettings.MainSchema);
        }
    }
}
