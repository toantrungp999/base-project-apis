﻿using System.Collections.Generic;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Extensions;
using BaseProject.Infrastructure.Persistence.Configurations;
using BaseProject.Infrastructure.Persistence.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BaseProject.Domain.Context
{
    public class ContextFactory : IContextFactory<BaseProjectContext>
    {
        private readonly DbContextOptions<BaseProjectContext> _contextOptions;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEnumerable<IEntityTypeConfigurationDependency> _entityTypeConfigurations;
        private readonly IOptions<AppSettings> _appSettings;

        public ContextFactory(DbContextOptions<BaseProjectContext> contextOptions,
            IHttpContextAccessor httpContextAccessor,
            IEnumerable<IEntityTypeConfigurationDependency> entityTypeConfigurations,
            IOptions<AppSettings> appSettings)
        {
            _contextOptions = contextOptions;
            _httpContextAccessor = httpContextAccessor;
            _entityTypeConfigurations = entityTypeConfigurations;
            _appSettings = appSettings;
        }

        public BaseProjectContext Create()
        {
            return new BaseProjectContext(_contextOptions, _entityTypeConfigurations, _appSettings, null, _httpContextAccessor.HttpContext.GetTimeZone());
        }
    }
}
