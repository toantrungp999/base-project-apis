﻿using BaseProject.Domain.Entities;
using BaseProject.Infrastructure.Persistence.Configurations;
using BaseProject.Infrastructure.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using BaseProject.Infrastructure.Configurations;
using Microsoft.Extensions.Options;
using System;

namespace BaseProject.Domain.Context
{
    public class BaseProjectContext : BaseDbContext
    {
        private readonly AppSettings _appSettings;

        /// <summary>
        /// This constructor is using for generating code 1st migration.
        /// </summary>
        public BaseProjectContext(DbContextOptions contextOptions, IOptions<AppSettings> appSettings)
            : base(contextOptions, new List<IEntityTypeConfigurationDependency>(), null, string.Empty)
        {
            _appSettings = appSettings.Value;
        }

        public BaseProjectContext(DbContextOptions contextOptions, IEnumerable<IEntityTypeConfigurationDependency> entityTypeConfigurations,
            IOptions<AppSettings> appSettings, Guid? userId, string requestedTimeZone)
            : base(contextOptions, entityTypeConfigurations, userId, requestedTimeZone)
        {
            _appSettings = appSettings.Value;
        }

        public DbSet<Attachment> Attachments { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Entity> Entitys { get; set; }

        public DbSet<Fanpage> Fanpages { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<History> Historys { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Messenger> Messengers { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.UserName)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.PhoneNumber)
                .IsUnique(true);

            modelBuilder.HasDefaultSchema(_appSettings.SchemaSettings.MainSchema);
        }
    }
}
