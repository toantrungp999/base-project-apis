﻿using BaseProject.Infrastructure.Persistence.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BaseProject.Domain.Entities
{
    public class Group : BaseEntity, IKey<Guid>
    {
        public Guid Id { get; set; }

        public string GroupName { get; set; }

        public int NumberOfMembers { get; set; }

        public Guid Owner { get; set; }

        [ForeignKey("Owner")]
        public virtual Entity Entity { get; set; }
    }
}
