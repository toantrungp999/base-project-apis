﻿using BaseProject.Infrastructure.Persistence.Base;
using System;

namespace BaseProject.Domain.Entities
{
    public class Attachment : BaseEntity, IKey<Guid>
    {
        public Guid ReferenceId { get; set; }

        public Guid Id { get; set; }

        public Guid FileId { get; set; }

        public string FileName { get; set; }

        public string FileDescription { get; set; }

        public string Extension { get; set; }

        public string Url { get; set; }

       public long FileSize { get; set; }
    }
}
