﻿using BaseProject.Infrastructure.Persistence.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BaseProject.Domain.Entities
{
    public class Messenger : BaseEntity, IKey<Guid>
    {
        public Guid Id { get; set; }

        [ForeignKey("Owner_1")]
        public Guid Owner_1 { get; set; }

        [ForeignKey("Owner_2")]
        public Guid Owner_2 { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual Entity Entity { get; set; }
    }
}
