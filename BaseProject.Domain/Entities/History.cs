﻿using BaseProject.Infrastructure.Persistence.Base;
using System;

namespace BaseProject.Domain.Entities
{
    public class History : IKey<Guid>
    {
        public Guid Id { get; set; }

        public string EntityName { get; set; }

        public Guid EntityId { get; set; }

        public string Action { get; set; }

        public DateTime ActionAt { get; set; }

        public string ActionBy { get; set; }

        public string Description { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }
    }
}
