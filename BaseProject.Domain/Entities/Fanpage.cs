﻿using System;
using BaseProject.Infrastructure.Persistence.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace BaseProject.Domain.Entities
{
    public class Fanpage : BaseEntity, IKey<Guid>
    {
        public Guid Id { get; set; }

        public string FanpageName { get; set; }
        
        public Guid Owner { get; set; }

        [ForeignKey("Owner")]
        public virtual Entity Entity { get; set; }
    }
}
