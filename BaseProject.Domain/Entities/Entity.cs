﻿using System;
using System.Collections.Generic;
using BaseProject.Infrastructure.Persistence.Base;

namespace BaseProject.Domain.Entities
{
    public class Entity : IKey<Guid>
    {
        public Guid Id { get; set; }

        public string EntityName { get; set; }

        public Guid EntityId { get; set; }

        public virtual ICollection<Messenger> Messengers { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<Fanpage> Fanpages { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
