﻿using System;
using BaseProject.Infrastructure.Persistence.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace BaseProject.Domain.Entities
{
    public class Notification : BaseEntity, IKey<Guid>
    {
        public Guid Id { get; set; }

        public Guid Owner { get; set; }

        public string Content { get; set; }

        public bool IsSeen { get; set; }


        [ForeignKey("Owner")]
        public virtual Entity Entity { get; set; }
    }
}
