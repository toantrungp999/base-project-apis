﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BaseProject.Domain.Migrations
{
    public partial class UpdateUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                schema: "main",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                schema: "main",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Role",
                schema: "main",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Username",
                schema: "main",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                schema: "main",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Password",
                schema: "main",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Role",
                schema: "main",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Username",
                schema: "main",
                table: "Users");
        }
    }
}
