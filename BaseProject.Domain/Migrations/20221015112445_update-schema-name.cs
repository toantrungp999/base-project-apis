﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BaseProject.Domain.Migrations
{
    public partial class updateschemaname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "Users",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Posts",
                newName: "Posts",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Notifications",
                newName: "Notifications",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Messengers",
                newName: "Messengers",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Messages",
                newName: "Messages",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Historys",
                newName: "Historys",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Groups",
                newName: "Groups",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Fanpages",
                newName: "Fanpages",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Entitys",
                newName: "Entitys",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Comments",
                newName: "Comments",
                newSchema: "MainSchema");

            migrationBuilder.RenameTable(
                name: "Attachments",
                newName: "Attachments",
                newSchema: "MainSchema");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "Users",
                schema: "MainSchema",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "Posts",
                schema: "MainSchema",
                newName: "Posts");

            migrationBuilder.RenameTable(
                name: "Notifications",
                schema: "MainSchema",
                newName: "Notifications");

            migrationBuilder.RenameTable(
                name: "Messengers",
                schema: "MainSchema",
                newName: "Messengers");

            migrationBuilder.RenameTable(
                name: "Messages",
                schema: "MainSchema",
                newName: "Messages");

            migrationBuilder.RenameTable(
                name: "Historys",
                schema: "MainSchema",
                newName: "Historys");

            migrationBuilder.RenameTable(
                name: "Groups",
                schema: "MainSchema",
                newName: "Groups");

            migrationBuilder.RenameTable(
                name: "Fanpages",
                schema: "MainSchema",
                newName: "Fanpages");

            migrationBuilder.RenameTable(
                name: "Entitys",
                schema: "MainSchema",
                newName: "Entitys");

            migrationBuilder.RenameTable(
                name: "Comments",
                schema: "MainSchema",
                newName: "Comments");

            migrationBuilder.RenameTable(
                name: "Attachments",
                schema: "MainSchema",
                newName: "Attachments");
        }
    }
}
