﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BaseProject.Domain.Context;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Infrastructure.Constants;
using BaseProject.Infrastructure.Persistence.Configurations;
using BaseProject.Infrastructure.Persistence.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NetCore.AutoRegisterDi;


namespace BaseProject.Domain.Configurations
{
	public static class DependencyInjection
	{
		/// <summary>
		/// Configure the dependency injection for Domain.
		/// </summary>
		public static IServiceCollection ConfigureDomainDi(this IServiceCollection services, IConfiguration configuration)
		{
			ConfigureDatabase(services, configuration);

			services.RegisterAssemblyPublicNonGenericClasses()
				.Where(c => c.Name.EndsWith("Repository"))
				.AsPublicImplementedInterfaces();

			return services;
		}

		private static void ConfigureDatabase(IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<BaseProjectContext>(opts =>
				opts.UseSqlServer(configuration.GetValue<string>(ConfigurationConstant.DefaultConnection)));

			services.AddSingleton<IContextFactory<BaseDbContext>, ContextFactory>(sp =>
				new ContextFactory(new DbContextOptionsBuilder<BaseProjectContext>()
						.UseSqlServer(configuration.GetValue<string>(ConfigurationConstant.DefaultConnection)).Options,
					sp.GetService<IHttpContextAccessor>(), sp.GetService<IEnumerable<IEntityTypeConfigurationDependency>>(),
					sp.GetService<IOptions<AppSettings>>()));

			foreach (TypeInfo type in typeof(BaseProjectContext).Assembly.DefinedTypes
				.Where(t => !t.IsAbstract &&
							!t.IsGenericTypeDefinition &&
							(typeof(IEntityTypeConfigurationDependency).IsAssignableFrom(t))))
			{
				if (!typeof(IEntityTypeConfigurationDependency).IsAssignableFrom(type))
				{
					continue;
				}

				services.AddSingleton(typeof(IEntityTypeConfigurationDependency), type);
			}
		}
	}
}
