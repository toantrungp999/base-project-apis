﻿using System;
using Microsoft.AspNetCore.Http;

namespace BaseProject.Infrastructure.JsonConverters
{
	public class DateTimeWithTimezoneConverter : BaseDateTimeWithTimeZoneConverter<DateTime>
	{
		public DateTimeWithTimezoneConverter(IHttpContextAccessor httpContextAccessor)
		{
			HttpContextAccessor = httpContextAccessor;
		}
	}
}
