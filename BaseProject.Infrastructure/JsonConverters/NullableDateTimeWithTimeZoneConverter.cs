﻿using System;
using Microsoft.AspNetCore.Http;

namespace BaseProject.Infrastructure.JsonConverters
{
	public class NullableDateTimeWithTimeZoneConverter : BaseDateTimeWithTimeZoneConverter<DateTime?>
	{
		public NullableDateTimeWithTimeZoneConverter(IHttpContextAccessor httpContextAccessor)
		{
			HttpContextAccessor = httpContextAccessor;
		}
	}
}
