﻿using System;
using System.Globalization;

namespace BaseProject.Infrastructure.Extensions
{
	public static class DateTimeExtension
	{
		/// <summary>
		///  Parse DateTime to exact format. By default the invariant culture will be applied.
		/// </summary>
		public static DateTime ParseExact(this string dateTime, string format, IFormatProvider formatProvider = null)
		{
			return DateTime.ParseExact(dateTime, format, formatProvider ?? CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Compare the current DateTime's year with an input year.
		/// </summary>
		public static bool EqualToYear(this DateTime? datetime, int year)
		{
			return datetime != null && datetime.Value.Year == year;
		}

		/// <summary>
		/// Compare the current DateTime's month with an input month.
		/// </summary>
		public static bool EqualToMonth(this DateTime? datetime, int month)
		{
			return datetime != null && datetime.Value.Month == month;
		}
	}
}
