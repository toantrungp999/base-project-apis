﻿namespace BaseProject.Infrastructure.Configurations
{
	public class AppSettings
	{
		public SchemaSettings SchemaSettings { get; set; }

		public string Secret { get; set; }
	}
}
