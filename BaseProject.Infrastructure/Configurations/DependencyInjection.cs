﻿using BaseProject.Infrastructure.Persistence.Context;
using BaseProject.Infrastructure.Persistence.Repositories;
using BaseProject.Infrastructure.Persistence.UnitOfWork;
using BaseProject.Infrastructure.Persistence.UnitOfWork.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BaseProject.Infrastructure.Configurations
{
	public static class DependencyInjection
	{
		/// <summary>
		/// Configure the dependency injection for Infrastructure.
		/// </summary>
		public static IServiceCollection ConfigureInfrastructureDi(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

			services.AddScoped<IUnitOfWorkProvider, UnitOfWorkProvider>(
				imp => new UnitOfWorkProvider(imp.GetService<IContextFactory<BaseDbContext>>()));

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			services.Configure<AppSettings>(opts => configuration.GetSection(nameof(AppSettings)).Bind(opts));

			return services;
		}
	}
}
