﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BaseProject.Infrastructure.Persistence.Extensions
{
	public static class ModelBuilderExtension
	{
		public static ModelBuilder EntitiesOfType<TType>(this ModelBuilder modelBuilder,
			Action<EntityTypeBuilder> buildAction) where TType : class
		{
			Type type = typeof(TType);

			foreach (var entityType in modelBuilder.Model.GetEntityTypes())
			{
				if (type.IsAssignableFrom(entityType.ClrType))
					buildAction(modelBuilder.Entity(entityType.ClrType));
			}

			return modelBuilder;
		}
	}
}
