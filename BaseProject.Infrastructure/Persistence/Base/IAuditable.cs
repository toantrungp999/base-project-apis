﻿using System;

namespace BaseProject.Infrastructure.Persistence.Base
{
	public interface IAuditable
	{
		DateTime? CreatedAt { get; set; }

		Guid? CreatedBy { get; set; }

		DateTime? UpdatedAt { get; set; }

		Guid? UpdatedBy { get; set; }
	}
}
