﻿using System;

namespace BaseProject.Infrastructure.Persistence.Base
{
	public abstract class BaseEntity : IDeletable, IAuditable
	{
		public bool IsDeleted { get; set; }

		public DateTime? DeletedAt { get; set; }

		public Guid? DeletedBy { get; set; }

		public DateTime? CreatedAt { get; set; }

		public Guid? CreatedBy { get; set; }

		public DateTime? UpdatedAt { get; set; }

		public Guid? UpdatedBy { get; set; }
	}
}
