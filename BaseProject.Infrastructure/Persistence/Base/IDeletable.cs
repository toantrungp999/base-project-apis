﻿using System;

namespace BaseProject.Infrastructure.Persistence.Base
{
	public interface IDeletable
	{
		bool IsDeleted { get; set; }

		DateTime? DeletedAt { get; set; }

		Guid? DeletedBy { get; set; }
	}
}
