﻿namespace BaseProject.Infrastructure.Persistence.UnitOfWork.Interfaces
{
	public interface IUnitOfWorkProvider
	{
		/// <summary>
		/// Provide a new unit of work scope.
		/// </summary>
		IUnitOfWorkScope Provide();

		/// <summary>
		/// Provide a new unit of work scope.
		/// </summary>
		/// <param name="scopeOption">
		/// Determine scope's option when creating a scope.
		/// </param>
		IUnitOfWorkScope Provide(UnitOfWorkScopeOption scopeOption);
	}
}
