﻿using System;
using System.Threading.Tasks;

namespace BaseProject.Infrastructure.Persistence.UnitOfWork.Interfaces
{
	public interface IBaseUnitOfWorkScope : IDisposable
	{
		/// <summary>
		/// Hold the reference to the outer scope.
		/// </summary>
		IBaseUnitOfWorkScope Parent { get; }

		/// <summary>
		/// Complete the current scope and send all done works to the database.
		/// </summary>
		void Complete();

		/// <summary>
		/// Send all done works to the database.
		/// </summary>
		int SaveChanges();

		/// <summary>
		/// Send asynchronously all done works to the database.
		/// </summary>
		Task<int> SaveChangesAsync();
	}
}
