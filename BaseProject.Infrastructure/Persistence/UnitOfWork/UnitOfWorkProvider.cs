﻿using BaseProject.Infrastructure.Persistence.Context;
using BaseProject.Infrastructure.Persistence.UnitOfWork.Interfaces;

namespace BaseProject.Infrastructure.Persistence.UnitOfWork
{
	public class UnitOfWorkProvider	: IUnitOfWorkProvider
	{
		private readonly IContextFactory<BaseDbContext> _contextFactory;

		public UnitOfWorkProvider(IContextFactory<BaseDbContext> contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public IUnitOfWorkScope Provide()
		{
			return Provide(UnitOfWorkScopeOption.Required);
		}

		public IUnitOfWorkScope Provide(UnitOfWorkScopeOption scopeOption)
		{
			return new UnitOfWorkScope(_contextFactory, scopeOption);
		}
	}
}
