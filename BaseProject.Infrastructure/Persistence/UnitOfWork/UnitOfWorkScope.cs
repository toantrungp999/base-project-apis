﻿using BaseProject.Infrastructure.Persistence.Context;
using BaseProject.Infrastructure.Persistence.UnitOfWork.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BaseProject.Infrastructure.Persistence.UnitOfWork
{
	public class UnitOfWorkScope : BaseUnitOfWorkScope<BaseDbContext, UnitOfWorkScope>, IUnitOfWorkScope
	{
		private BaseDbContext _dbContext;
	    private readonly IContextFactory<BaseDbContext> _contextFactory;

        #region Constructors

        public UnitOfWorkScope(IContextFactory<BaseDbContext> contextFactory)
            : this(contextFactory, UnitOfWorkScopeOption.Required)
        {
        }

        public UnitOfWorkScope(IContextFactory<BaseDbContext> contextFactory, UnitOfWorkScopeOption scopeOption)
        {
	        _contextFactory = contextFactory;
            InitializeScope(scopeOption);
        }

        #endregion Constructors

        #region Public Methods

        public void Evict<T>(T obj)
        {
            if (DbContext.Entry(obj).State != EntityState.Detached)
            {
                DbContext.Entry(obj).State = EntityState.Detached;
            }
        }

        public void Attach<T>(T obj)
        {
            if (obj != null && DbContext.Entry(obj).State == EntityState.Detached)
            {
                DbContext.Entry(obj).State = EntityState.Unchanged;
            }
        }

        public EntityState? GetState<T>(T obj)
        {
            if (obj != null)
            {
                return DbContext.Entry(obj).State;
            }

            return null;
        }

        #endregion Public Methods

        #region Override Methods

        public override BaseDbContext DbContext => _dbContext;

        protected override void CreateContext()
        {
	        _dbContext = _contextFactory.Create();
	        CreateTransaction();
        }

        protected override void InheritContext()
        {
	        _dbContext = Current.DbContext;
        }

        #endregion Override Methods

        #region Private Methods

        private void CreateTransaction()
        {
	        CurrentTransaction = DbContext.Database.BeginTransaction();
        }

        #endregion
	}
}
