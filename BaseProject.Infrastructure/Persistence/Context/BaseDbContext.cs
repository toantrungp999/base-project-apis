﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using BaseProject.Infrastructure.Persistence.Base;
using BaseProject.Infrastructure.Persistence.Configurations;
using BaseProject.Infrastructure.Persistence.Extensions;
using BaseProject.Infrastructure.Persistence.Translations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace BaseProject.Infrastructure.Persistence.Context
{
	public abstract class BaseDbContext : DbContext
	{
		private readonly Guid? _userId;
		private readonly string _requestedTimeZone;
		private readonly IEnumerable<IEntityTypeConfigurationDependency> _entityTypeConfigurations;

		protected BaseDbContext(DbContextOptions contextOptions,
			IEnumerable<IEntityTypeConfigurationDependency> entityTypeConfigurations,
			Guid? userId, string requestedTimeZone) : base(contextOptions)
		{
			_entityTypeConfigurations = entityTypeConfigurations;
			_userId = userId;
			_requestedTimeZone = requestedTimeZone;
		}

        [Obsolete]
        protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.EntitiesOfType<IDeletable>(builder =>
			{
				if (builder.Metadata.BaseType != null)
				{
					return;
				}

				ParameterExpression clrParameter = Expression.Parameter(builder.Metadata.ClrType);
				MemberExpression isDeleted = Expression.Property(clrParameter, nameof(IDeletable.IsDeleted));
				BinaryExpression comparer = Expression.Equal(isDeleted, Expression.Constant(false));
				builder.HasQueryFilter(Expression.Lambda(comparer, clrParameter));
			});

			foreach (IEntityTypeConfigurationDependency entityTypeConfiguration in _entityTypeConfigurations)
			{
				entityTypeConfiguration.Configure(modelBuilder);
			}

			modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Translate(_requestedTimeZone);
		}

		public override int SaveChanges(bool acceptAllChangesOnSuccess)
		{
			BeforeSaving();
			return base.SaveChanges(acceptAllChangesOnSuccess);
		}

		public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, 
			CancellationToken cancellationToken = new CancellationToken())
		{
			BeforeSaving();
			return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
		}

		private void BeforeSaving()
		{
			var entries = ChangeTracker.Entries().ToList();

			foreach(var entry in entries)
			{
				if(entry.Entity is IAuditable auditableEntity)
				{
					var now = DateTime.UtcNow;

					switch(entry.State)
					{
						case EntityState.Modified:
							auditableEntity.UpdatedAt = now;
							auditableEntity.UpdatedBy = _userId;
							break;

						case EntityState.Added:
							auditableEntity.CreatedAt = now;
							auditableEntity.CreatedBy = _userId;
							auditableEntity.UpdatedAt = now;
							auditableEntity.UpdatedBy = _userId;
							break;

						case EntityState.Deleted:
							DeleteEntry(entry, now, _userId);
							break;

						case EntityState.Detached:
						case EntityState.Unchanged:
						default:
							break;
					}
				}
			}
		}

		private void DeleteEntry(EntityEntry entry, DateTime deletedDate, Guid? userId)
		{
			if(!(entry is {Entity: IDeletable deletableEntity}))
			{
				return;
			}

			entry.State = EntityState.Modified;
			deletableEntity.IsDeleted = true;
			deletableEntity.DeletedAt = deletedDate;
			deletableEntity.DeletedBy = userId;
		}
	}
}
