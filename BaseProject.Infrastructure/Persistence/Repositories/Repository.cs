﻿using System;
using System.Collections.Generic;
using System.Linq;
using BaseProject.Infrastructure.Extensions;
using BaseProject.Infrastructure.Persistence.Context;
using BaseProject.Infrastructure.Persistence.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace BaseProject.Infrastructure.Persistence.Repositories
{
	public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
	{
		protected BaseDbContext DbContext => UnitOfWorkScope.Current.DbContext;

		public TEntity Get(Guid id)
		{
			return DbContext.Set<TEntity>().Find(id);
		}

		public void Add(TEntity entity)
		{
			DbContext.Set<TEntity>().Add(entity);
		}

		public void AddRange(IList<TEntity> entities)
		{
			DbContext.Set<TEntity>().AddRange(entities);
		}

		public void Update(TEntity entity)
		{
			DbContext.Set<TEntity>().Update(entity);
		}

		public void UpdateRange(IList<TEntity> entities)
		{
			DbContext.Set<TEntity>().UpdateRange(entities);
		}

		public void Delete(TEntity entity)
		{
			DbContext.Set<TEntity>().Remove(entity);
		}

		public void DeleteRange(IList<TEntity> entities)
		{
			DbContext.Set<TEntity>().RemoveRange(entities);
		}

		public IQueryable<TEntity> GetAll()
		{
			return GetAll(false, null);
		}

		public IQueryable<TEntity> GetAll(bool isUntrackEntities)
		{
			return GetAll(isUntrackEntities, null);
		}

		public IQueryable<TEntity> GetAll(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeFn)
		{
			return GetAll(false, includeFn);
		}

		public IQueryable<TEntity> GetAll(bool isUntrackEntities, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeFn)
		{
			IQueryable<TEntity> query = DbContext.Set<TEntity>();

			if (isUntrackEntities)
			{
				query.AsNoTracking();
			}

			if (includeFn != null)
			{
				query = includeFn(query);
			}

			return query;
		}

		public void ExecuteRawSql(string sql, params object[] parameters)
		{
			if (parameters.IsNullOrEmpty())
			{
				DbContext.Database.ExecuteSqlRaw(sql);

				return;
			}

			DbContext.Database.ExecuteSqlRaw(sql, parameters);
		}

		public IQueryable<TEntity> FromSqlRaw(string query)
		{
			return DbContext.Set<TEntity>().FromSqlRaw(query);
		}
	}
}
