﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;

namespace BaseProject.Infrastructure.Persistence.Repositories
{
	public interface IRepository<TEntity>
	{
		TEntity Get(Guid id);

		void Add(TEntity entity);

		void AddRange(IList<TEntity> entities);

		void Update(TEntity entity);

		void UpdateRange(IList<TEntity> entities);

		void Delete(TEntity entity);

		void DeleteRange(IList<TEntity> entities);

		IQueryable<TEntity> GetAll();

		IQueryable<TEntity> GetAll(bool isUntrackEntities);

		IQueryable<TEntity> GetAll(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeFn);

		IQueryable<TEntity> GetAll(bool isUntrackEntities, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeFn);

		void ExecuteRawSql(string sql, params object[] parameters);

		IQueryable<TEntity> FromSqlRaw(string query);
	}
}
