﻿using System.Linq.Expressions;
using BaseProject.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Storage;
using NodaTime;

namespace BaseProject.Infrastructure.Persistence.Translations.DateTime
{
    public abstract class BaseSqlBinaryDateTimeOffsetExpression : SqlBinaryExpression
    {
        [System.Obsolete]
        protected BaseSqlBinaryDateTimeOffsetExpression(SqlExpression left, SqlExpression right, string datePart, string requestTimeZone)
            : base(ExpressionType.Equal, ExtractDatePartFromDateExpr(left, datePart, requestTimeZone), right, right.Type, right.TypeMapping)
        {
        }

        [System.Obsolete]
        private static SqlExpression ExtractDatePartFromDateExpr(SqlExpression left, string datePart, string requestTimeZone)
        {
            int offset = 0;
            string hourPart = "hh";

            if (!string.IsNullOrEmpty(requestTimeZone))
            {
                var timeZone = DateTimeZoneProviders.Tzdb[requestTimeZone.Trim()];

                var utcOffset = timeZone.GetUtcOffset(SystemClock.Instance.GetCurrentInstant());

                offset = int.TryParse(utcOffset.ToString(), out var result) ? result : default;
            }

            var specifiedLeftExpr = SqlFunctionExpression.Create(DatabaseConstant.DateAddFunc, new SqlExpression[]
            {
                new SqlFragmentExpression(hourPart),
                new SqlConstantExpression(Constant(offset), new IntTypeMapping(DatabaseConstant.IntType)),
                left
            }, left.Type, left.TypeMapping);

            var datePartExpr = new SqlFragmentExpression(datePart);

            var leftExpr = SqlFunctionExpression.Create(DatabaseConstant.DatePartFunc, new SqlExpression[]
                {
                    datePartExpr,
                    specifiedLeftExpr
                },
                specifiedLeftExpr.Type, specifiedLeftExpr.TypeMapping);

            return leftExpr;
        }
    }
}
