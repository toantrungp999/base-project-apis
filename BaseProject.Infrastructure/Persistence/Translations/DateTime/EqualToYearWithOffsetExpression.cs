﻿using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace BaseProject.Infrastructure.Persistence.Translations.DateTime
{
	/// <summary>
	/// Translate the in-memory function to the sql query.
	/// E.g. (DATEPART(year, DATEADD(hh, UTC Offset, [Schema].[Column])) = Search Year
	/// </summary>
	public class EqualToYearWithOffsetExpression : BaseSqlBinaryDateTimeOffsetExpression
	{
		public EqualToYearWithOffsetExpression(SqlExpression left, SqlExpression right, string requestTimeZone)
			: base(left, right, "year", requestTimeZone)
		{
		}
	}
}
