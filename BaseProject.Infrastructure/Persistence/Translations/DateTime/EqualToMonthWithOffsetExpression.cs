﻿using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace BaseProject.Infrastructure.Persistence.Translations.DateTime
{
	/// <summary>
	/// Translate the in-memory function to the sql query.
	/// E.g. (DATEPART(month, DATEADD(hh, UTC Offset, [Schema].[Column])) = Search Month
	/// </summary>
	public class EqualToMonthWithOffsetExpression : BaseSqlBinaryDateTimeOffsetExpression
	{
		public EqualToMonthWithOffsetExpression(SqlExpression left, SqlExpression right, string requestTimeZone)
			: base(left, right, "month", requestTimeZone)
		{
		}
	}
}
