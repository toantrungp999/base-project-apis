﻿using System.Linq.Expressions;
using BaseProject.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Storage;

namespace BaseProject.Infrastructure.Persistence.Translations.String
{
	/// <summary>
	/// Translate the in-memory function to the sql query.
	/// E.g. (LOWER([Schema].[Column]) = LOWER(N'Search Term')
	/// </summary>
	public class EqualsInvariantExpression : BaseSqlBinaryStringExpression
	{
		public EqualsInvariantExpression(SqlExpression left, SqlExpression right) 
			: base(ExpressionType.Equal, BuildLowerExpr(left), BuildLowerExpr(right), typeof(string), new StringTypeMapping(DatabaseConstant.StringVarcharType))
		{
		}
	}
}
