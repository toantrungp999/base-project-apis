﻿using System;
using System.Linq.Expressions;
using BaseProject.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Storage;

namespace BaseProject.Infrastructure.Persistence.Translations.String
{
	public abstract class BaseSqlBinaryStringExpression	: SqlBinaryExpression
	{
		protected BaseSqlBinaryStringExpression(ExpressionType operatorType, SqlExpression left, SqlExpression right, Type type, RelationalTypeMapping typeMapping) 
			: base(operatorType, left, right, type, typeMapping)
		{
		}

        [Obsolete]
        protected static SqlExpression BuildLowerExpr(SqlExpression exp)
		{
			return SqlFunctionExpression.Create(DatabaseConstant.LowerFunc, new[] {exp}, typeof(string), new StringTypeMapping(DatabaseConstant.StringVarcharType));
		}
	}
}
