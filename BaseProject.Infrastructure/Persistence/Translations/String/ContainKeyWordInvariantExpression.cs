﻿using BaseProject.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq.Expressions;

namespace BaseProject.Infrastructure.Persistence.Translations.String
{
	/// <summary>
	/// Translate the in-memory function to the sql query.
	/// E.g. CHARINDEX(LOWER([Schema].[Column]), LOWER(N'Search Term')) > 0
	/// </summary>
	public class ContainKeyWordInvariantExpression : BaseSqlBinaryStringExpression
	{
        [System.Obsolete]
        public ContainKeyWordInvariantExpression(SqlExpression left, SqlExpression right)
			: base(ExpressionType.GreaterThan, BuildStringContainExpr(left, right), 
				BuildConstantZeroExpr(), typeof(string), new StringTypeMapping(DatabaseConstant.StringVarcharType))
		{
		}

		private static SqlExpression BuildConstantZeroExpr()
		{
			return new SqlConstantExpression(Constant(0), new IntTypeMapping(DatabaseConstant.IntType));
		}

        [System.Obsolete]
        private static SqlExpression BuildStringContainExpr(SqlExpression left, SqlExpression right)
		{
			return SqlFunctionExpression.Create(DatabaseConstant.StringContainFunc, new [] { BuildLowerExpr(right), BuildLowerExpr(left) },
				typeof(string), new StringTypeMapping(DatabaseConstant.StringVarcharType));
		}
	}
}
