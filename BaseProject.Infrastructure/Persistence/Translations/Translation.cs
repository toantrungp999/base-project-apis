﻿using System.Linq;
using System.Reflection;
using BaseProject.Infrastructure.Extensions;
using BaseProject.Infrastructure.Persistence.Translations.DateTime;
using BaseProject.Infrastructure.Persistence.Translations.String;
using Microsoft.EntityFrameworkCore;

namespace BaseProject.Infrastructure.Persistence.Translations
{
	/// <summary>
	/// Translate all in-memory functions into sql query.
	/// </summary>
	public static class Translation
	{
        [System.Obsolete]
        public static void Translate(this ModelBuilder modelBuilder, string requestedTimeZone)
		{
			TranslateEqualsInvariantFunc(modelBuilder);
			TranslateContainKeyWordInvariantFunc(modelBuilder);
			TranslateEqualToMonthWithOffsetFunc(modelBuilder, requestedTimeZone);
			TranslateEqualToYearWithOffsetFunc(modelBuilder, requestedTimeZone);
		}

		private static void TranslateEqualsInvariantFunc(ModelBuilder modelBuilder)
		{
			MethodInfo methodInfo = typeof(StringExtension).GetMethod(nameof(StringExtension.EqualsInvariant));

			if (methodInfo != null)
			{
				modelBuilder.HasDbFunction(methodInfo)
					.HasTranslation(args => new EqualsInvariantExpression(args.First(), args.Last()));
			}
		}

        [System.Obsolete]
        private static void TranslateContainKeyWordInvariantFunc(ModelBuilder modelBuilder)
		{
			MethodInfo methodInfo = typeof(StringExtension).GetMethod(nameof(StringExtension.ContainKeyWordInvariant));

			if (methodInfo != null)
			{
				modelBuilder.HasDbFunction(methodInfo)
					.HasTranslation(args => new ContainKeyWordInvariantExpression(args.First(), args.Last()));
			}
		}

		private static void TranslateEqualToMonthWithOffsetFunc(ModelBuilder modelBuilder, string requestedTimeZone)
		{
			MethodInfo methodInfo = typeof(DateTimeExtension).GetMethod(nameof(DateTimeExtension.EqualToMonth));

			if (methodInfo != null)
			{
				modelBuilder.HasDbFunction(methodInfo)
					.HasTranslation(args => new EqualToMonthWithOffsetExpression(args.First(), args.Last(), requestedTimeZone));
			}
		}

		private static void TranslateEqualToYearWithOffsetFunc(ModelBuilder modelBuilder, string requestedTimeZone)
		{
			MethodInfo methodInfo = typeof(DateTimeExtension).GetMethod(nameof(DateTimeExtension.EqualToYear));

			if (methodInfo != null)
			{
				modelBuilder.HasDbFunction(methodInfo)
					.HasTranslation(args => new EqualToYearWithOffsetExpression(args.First(), args.Last(), requestedTimeZone));
			}
		}
	}
}
