﻿namespace BaseProject.Infrastructure.Constants
{
	public static class ConfigurationConstant
	{
		public const string LoggingSection = "Logging";

		public const string TimeZoneKey = "X-Timezone-Offset";

		public const string DefaultAppSettings = "appsettings.json";

		public const string SecretKey = "AppSettings:Secret";

		public const string DefaultApiVersion = "1.0";

		public const string DefaultContentType = "application/json";

		public const string DefaultConnection = "ConnectionString:DefaultConnection";

		public const string Swagger = nameof(Swagger);
		public const string SwaggerSettings = nameof(SwaggerSettings);
		public const string SwaggerAuthorizationDescription =
			"JWT Authorization header using the Bearer scheme. Example: \"Bearer {Token}\"";
		public const string AuthorizationHeader = "Authorization";

		public const string CorsPolicy = nameof(CorsPolicy);
		public const string AllowedOrigins = "Cors:AllowedOrigins";
	}
}
