﻿namespace BaseProject.Infrastructure.Constants
{
	public static class DatabaseConstant
	{
		#region Functions

		public const string LowerFunc = "LOWER";

		public const string StringContainFunc = "CHARINDEX";

		public const string DatePartFunc = "DATEPART";

		public const string DateAddFunc = "DATEADD";

		#endregion

		#region Types

		public const string StringVarcharType = "VARCHAR";

		public const string IntType = "INT";

		#endregion
	}
}
