﻿using System.ComponentModel;

namespace BaseProject.Infrastructure.Enums
{
    public enum Sex
    {
        [Description("Male")]
        Male = 1,

        [Description("Female")]
        Female,

        [Description("Other")]
        Other
    }
}
