﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BaseProject.APIs.Responses
{
	public class ErrorResponseWrapper
	{
		[JsonProperty("errors")]
		public IList<ErrorResponse> Errors { get; set; }
	}
}
