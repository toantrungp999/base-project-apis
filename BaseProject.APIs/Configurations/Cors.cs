﻿using BaseProject.Infrastructure.Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BaseProject.APIs.Configurations
{
	/// <summary>
	/// Allow Cross-Origin Resource Sharing.
	/// </summary>
	public static class Cors
	{
		public static void AllowCors(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddCors(options =>
			{
				options.AddPolicy(ConfigurationConstant.CorsPolicy,
					builders =>
					{
						builders
							.WithOrigins(configuration.GetSection(ConfigurationConstant.AllowedOrigins).Get<string[]>())
							.AllowAnyHeader()
							.AllowAnyMethod()
							.AllowCredentials();
					});
			});
		}
	}
}
