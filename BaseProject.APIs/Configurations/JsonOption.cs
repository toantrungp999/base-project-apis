﻿using BaseProject.Infrastructure.JsonConverters;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace BaseProject.APIs.Configurations
{
	public static class JsonOptions
	{
		/// <summary>
		/// Configure the Json auto serialization with some customization.
		/// </summary>
		public static IMvcBuilder ConfigureJsonOptions(this IMvcBuilder builder)
		{
			var sp = builder.Services.BuildServiceProvider();

			var httpContextAccessor = sp.GetService(typeof(IHttpContextAccessor)) as IHttpContextAccessor;

			builder.AddNewtonsoftJson(cfg =>
			{
				cfg.SerializerSettings.Converters.Add(new DateTimeWithTimezoneConverter(httpContextAccessor));
				cfg.SerializerSettings.Converters.Add(new NullableDateTimeWithTimeZoneConverter(httpContextAccessor));
			});            

			return builder;
		}
	}
}
