﻿using System.Collections.Generic;
using BaseProject.APIs.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;

namespace BaseProject.APIs.Configurations
{
	/// <summary>
	/// API versioning error provider.
	/// </summary>
	public class ApiVersioningErrorResponseProvider : DefaultErrorResponseProvider
	{
		public override IActionResult CreateResponse(ErrorResponseContext context)
		{
			var errorWrapper = new ErrorResponseWrapper
			{
				Errors = new List<ErrorResponse>
				{
					new ErrorResponse
					{
						Message = context.Message,
						MessageDetail = context.MessageDetail
					}
				}
			};

			return new ObjectResult(errorWrapper) { StatusCode = context.StatusCode };
		}
	}
}
