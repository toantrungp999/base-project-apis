﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using BaseProject.APIs.Responses;
using BaseProject.Infrastructure.Constants;
using BaseProject.Infrastructure.Exceptions;
using BaseProject.Infrastructure.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BaseProject.APIs.Configurations
{
	/// <summary>
	/// Middleware for the global exception handler.
	/// </summary>
	public class CustomExceptionHandler
	{
		private readonly RequestDelegate _next;
		private readonly ILogger<CustomExceptionHandler> _logger;

		public CustomExceptionHandler(RequestDelegate next, ILogger<CustomExceptionHandler> logger)
		{
			_next = next;
			_logger = logger;
		}

		public async Task Invoke(HttpContext httpContext)
		{
			if (httpContext.HasSwaggerRequest())
			{
				await _next(httpContext);
				return;
			}

			try
			{
				await _next(httpContext);
			}
			catch (Exception ex)
			{
				await HandleException(httpContext, ex);
			}
		}

		private async Task HandleException(HttpContext context, Exception exception)
		{
			context.Response.ContentType = ConfigurationConstant.DefaultContentType;
			switch (exception)
			{
				case BusinessLogicException _:
					context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
					break;

				case NotFoundException _:
					context.Response.StatusCode = (int)HttpStatusCode.NotFound;
					break;

				case UnsupportedTypeException _:
					context.Response.StatusCode = (int)HttpStatusCode.UnsupportedMediaType;
					break;

				case UnauthorizedAccessException _:
					context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
					break;

				default:
					_logger.LogError(exception, exception.Message);
					context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					break;
			}

			ErrorResponseWrapper errorResponseWrapper = new ErrorResponseWrapper
			{
				Errors = new List<ErrorResponse>
				{
					new ErrorResponse
					{
						Message = exception.Message,
						MessageDetail = exception.InnerException?.Message
					}
				}
			};

			await context.Response.WriteAsync(JsonConvert.SerializeObject(errorResponseWrapper));
		}
	}
}
