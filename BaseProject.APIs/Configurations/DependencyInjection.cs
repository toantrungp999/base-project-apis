﻿using BaseProject.Domain.Configurations;
using BaseProject.Infrastructure.Configurations;
using BaseProject.Services.Configurations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BaseProject.APIs.Configurations
{
	public static class DependencyInjection
	{
		/// <summary>
		/// Configure the dependency injection for entire solution.
		/// </summary>
		public static void ConfigureDi(this IServiceCollection services, IConfiguration configuration)
		{
			services.ConfigureInfrastructureDi(configuration)
				.ConfigureDomainDi(configuration)
				.ConfigureServiceDi()
				.ConfigureOthers();
		}

		private static void ConfigureOthers(this IServiceCollection services)
		{
			services.AddTransient<IAuthorizationHandler, CustomIAuthorizationHandler>();
		}
	}
}
