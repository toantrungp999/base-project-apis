using BaseProject.APIs.Configurations;
using BaseProject.APIs.Extensions;
using BaseProject.Infrastructure.Constants;
using BaseProject.Services.Configurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BaseProject.APIs
{
	public class Startup
	{
		public Startup(IWebHostEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile(ConfigurationConstant.DefaultAppSettings, true, true);

			builder.AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.ConfigureDi(Configuration);

			services.AddControllers().ConfigureApiBehaviorOptions().ConfigureJsonOptions();

			services.ConfigureFluentValidation();

			services.ConfigureSwagger(Configuration);

			services.AllowCors(Configuration);

			services.ConfigureApiVersioning();

			services.ConfigureAutoMapper();

			services.ConfigureAuthorization(Configuration);
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseCors(ConfigurationConstant.CorsPolicy);

			app.UseCustomResponseWrapper();

			app.UseCustomExceptionHandler();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseSwagger();

			app.UseSwaggerUI(opts =>
			{
				var provider = app.ApplicationServices.GetService<IApiVersionDescriptionProvider>();

				foreach (var description in provider.ApiVersionDescriptions)
				{
					opts.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
				}
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});

			loggerFactory.AddFile(Configuration.GetSection(ConfigurationConstant.LoggingSection));
		}
	}
}
