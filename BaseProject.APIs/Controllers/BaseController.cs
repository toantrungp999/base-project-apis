﻿using BaseProject.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BaseProject.APIs.Controllers
{
	[Route("api/v{version:apiVersion}/[controller]s")]
	[ApiController]
	public class BaseController : ControllerBase
	{
		protected Guid? UserId => HttpContext.GetUserId();
		protected string AuthorizationToken => HttpContext.GetAuthorizationToken();
	}
}
