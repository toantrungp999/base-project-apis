﻿using System;
using BaseProject.Infrastructure.Constants;
using BaseProject.Services.Dto;
using BaseProject.Services.Dto.v1.UserDto;
using BaseProject.Services.Services.v1.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BaseProject.APIs.Controllers.v1
{
    [Authorize]
    [ApiVersion("1.0")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("UerInfo")]
        public IActionResult GetUserById()
        {
            var result = _userService.GetUserById((Guid)UserId);

            return Ok(result);
        }

        [Authorize(Roles = RoleConstant.Admin)]
        [HttpGet]
        public IActionResult GetUsers([FromQuery] PaginationRequestDto paginationRequest)
        {
            var result = _userService.GetUsers(paginationRequest);

            return Ok(result);
        }

        [HttpPost, Route("Authenticate")]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] AuthenticateRequestDto request)
        {
            var result = _userService.Authenticate(request);

            return Ok(result);
        }

        [HttpPost, Route("CreateUser")]
        [AllowAnonymous]
        public IActionResult CreateUser([FromBody] InsertUserRequestDto request)
        {
            var userId = _userService.InsertUser(request);

            return Created(HttpContext.Request.Path, new
            {
                UserId = userId
            });
        }
    }
}
